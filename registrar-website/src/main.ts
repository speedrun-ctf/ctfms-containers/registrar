import * as asn1js from "asn1js";
import * as pkijs from "pkijs";
import * as pvtsutils from "pvtsutils";

const crtgenForm = <HTMLFormElement> document.getElementById("crtgen");
const generateButton = <HTMLInputElement> document.getElementById("generate");
const usernameField = <HTMLInputElement> document.getElementById("username");
const emailField = <HTMLInputElement> document.getElementById("email");
// const passwordField = <HTMLInputElement> document.getElementById("password");

function download(buf: BufferSource, filename: string, type: string) {
    const pkcs12AsBlob = new Blob([buf], { type });
    const downloadLink = document.createElement("a");
    downloadLink.download = filename;
    downloadLink.innerHTML = "Download File";

    downloadLink.href = window.URL.createObjectURL(pkcs12AsBlob);
    downloadLink.onclick = () => document.body.removeChild(downloadLink);
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);

    downloadLink.click();
}

generateButton.onclick = async () => {
    generateButton.disabled = true;
    if (crtgenForm.checkValidity()) {
        let hashAlg = "sha-256";
        const csr = new pkijs.CertificationRequest();
        const crypto = pkijs.getCrypto(true);

        const username = usernameField.value;
        const email = emailField.value;
        // const password = passwordField.value;

        const algorithm = pkijs.getAlgorithmParameters("ECDSA", "generateKey") as any;
        if ("hash" in algorithm.algorithm)
            algorithm.algorithm.hash.name = hashAlg;

        const { privateKey, publicKey } = await crypto.generateKey(algorithm.algorithm, true, algorithm.usages) as Required<CryptoKeyPair>;

        csr.version = 2;
        csr.subject.typesAndValues.push(new pkijs.AttributeTypeAndValue({
            type: "2.5.4.3", // commonName
            value: new asn1js.BmpString({ value: username })
        }));
        csr.subject.typesAndValues.push(new pkijs.AttributeTypeAndValue({
            type: "1.2.840.113549.1.9.1", // emailAddress
            value: new asn1js.BmpString({ value: email })
        }));
        await csr.subjectPublicKeyInfo.importKey(publicKey);
        await csr.sign(privateKey, hashAlg);
        let resp = await fetch("sign", {
            method: "POST",
            headers: {
                'Content-Type': 'application/octet-stream'
            },
            body: csr.toSchema().toBER(false)
        });

        const cert = toPEM(await resp.arrayBuffer(), "CERTIFICATE");
        const pkcs8 = toPEM(await crypto?.subtle.exportKey("pkcs8", privateKey), "PRIVATE KEY");

        const enc = new TextEncoder();
        download(enc.encode(cert).buffer, username + ".pem", "application/x-pem-file");
        download(enc.encode(pkcs8).buffer, username + ".key", "application/x-pem-file");
    } else {
        generateButton.disabled = false;
    }
}

// modified from: https://github.com/PeculiarVentures/PKI.js/blob/e71b99c0b41c705c0ed27502161272c208a1ee33/test/utils.ts
// Copyright (c) 2014, GlobalSign
// Copyright (c) 2015-2019, Peculiar Ventures
// All rights reserved.
//
// Author 2014-2019, Yury Strozhevsky
function toPEM(buffer: BufferSource, tag: string): string {
    return [
        `-----BEGIN ${tag}-----`,
        formatPEM(pvtsutils.Convert.ToBase64(buffer)),
        `-----END ${tag}-----`,
        "",
    ].join("\n");
}

/**
 * Format string in order to have each line with length equal to 64
 * @param pemString String to format
 * @returns Formatted string
 */
function formatPEM(pemString: string): string {
    const PEM_STRING_LENGTH = pemString.length, LINE_LENGTH = 64;
    const wrapNeeded = PEM_STRING_LENGTH > LINE_LENGTH;

    if (wrapNeeded) {
        let formattedString = "", wrapIndex = 0;

        for (let i = LINE_LENGTH; i < PEM_STRING_LENGTH; i += LINE_LENGTH) {
            formattedString += pemString.substring(wrapIndex, i) + "\r\n";
            wrapIndex = i;
        }

        formattedString += pemString.substring(wrapIndex, PEM_STRING_LENGTH);
        return formattedString;
    }
    else {
        return pemString;
    }
}