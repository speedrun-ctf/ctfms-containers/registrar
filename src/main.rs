#![forbid(unused_must_use)]
#![forbid(unused_results)]

use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::ops::{Deref, DerefMut};
use std::path::PathBuf;
use std::str::FromStr;

use log::*;
use openssl::asn1::{Asn1Integer, Asn1Time};
use openssl::bn::BigNum;
use openssl::hash::MessageDigest;
use openssl::nid::Nid;
use openssl::pkey::{PKey, Private};
use openssl::x509::{X509Builder, X509Extension, X509NameBuilder, X509Req, X509};
use rand::{CryptoRng, Rng, SeedableRng};
use rand_chacha::ChaCha20Rng;
use tokio::fs::File;
use tokio::io::AsyncReadExt;
use tokio::net::TcpStream;
use tokio::sync::mpsc::{channel, Receiver};
use tokio::sync::oneshot;
use trust_dns_resolver::AsyncResolver;
use zeroize::Zeroizing;

use common::{
    await_message, send_message, Request, RequestResult, Secret, UserRegistration, Username,
};

#[cfg(not(feature = "web"))]
mod tui;
#[cfg(not(feature = "web"))]
use tui::run_server;

#[cfg(feature = "web")]
mod web;
#[cfg(feature = "web")]
use web::run_server;

const DEFAULT_PORT: u16 = 1111;
const DEFAULT_KEY_PATH: &str = "/key";
const DEFAULT_CA_PATH: &str = "/ca";

const FIXED_NIDS: [Nid; 4] = [
    Nid::COUNTRYNAME,
    Nid::STATEORPROVINCENAME,
    Nid::ORGANIZATIONNAME,
    Nid::ORGANIZATIONALUNITNAME,
];

fn default_saddr() -> IpAddr {
    IpAddr::V4(Ipv4Addr::from([0, 0, 0, 0]))
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    env_logger::builder()
        .filter_level(LevelFilter::Info)
        .parse_default_env()
        .init();

    let secret = hex::decode(
        std::env::var("RSECRET")
            .expect("Registrar secret (RSECRET) required for connecting to the central server."),
    )?
    .as_slice()
    .try_into()?;

    let central_addr = {
        let resolver = AsyncResolver::tokio_from_system_conf()?;
        resolver
            .lookup_ip("central")
            .await?
            .iter()
            .next()
            .expect("No IP found for central; failing fast.")
    };
    let central_port = u16::from_str(
        &std::env::var("CPORT")
            .expect("Central port (CPORT) required for connecting to the central server."),
    )
    .expect("Invalid CPORT.");
    let central = SocketAddr::new(central_addr, central_port);

    let port = match std::env::var("PORT") {
        Ok(user_provided) => u16::from_str(&user_provided)?,
        Err(_) => {
            warn!(
                target: "startup",
                "No port (PORT) env variable specified; defaulting to {}.",
                DEFAULT_PORT
            );
            DEFAULT_PORT
        }
    };
    let addr = match std::env::var("SADDR") {
        Ok(user_provided) => SocketAddr::new(IpAddr::from_str(&user_provided)?, port),
        Err(_) => {
            let default = default_saddr();
            info!(
                target: "startup",
                "No server address (SADDR) env variable specified; defaulting to {}",
                default
            );
            SocketAddr::new(default, port)
        }
    };
    let key_request_channel = {
        let key = PathBuf::from(std::env::var("KEY").unwrap_or_else(|_| {
            info!(
                target: "startup",
                "No key (KEY) env variable specified; defaulting to {}",
                DEFAULT_KEY_PATH
            );
            String::from(DEFAULT_KEY_PATH)
        }));
        let ca = PathBuf::from(std::env::var("CA").unwrap_or_else(|_| {
            info!(
                target: "startup",
                "No key (CA) env variable specified; defaulting to {}",
                DEFAULT_CA_PATH
            );
            String::from(DEFAULT_CA_PATH)
        }));

        let mut key = File::open(key).await?;
        let mut keybytes = Zeroizing::new(Vec::with_capacity(key.metadata().await?.len() as usize));
        let _ = key.read_to_end(keybytes.deref_mut()).await?;
        let key = PKey::private_key_from_pem(&keybytes)?;
        drop(keybytes);

        let mut ca = File::open(ca).await?;
        let mut cabytes = Zeroizing::new(Vec::with_capacity(ca.metadata().await?.len() as usize));
        let _ = ca.read_to_end(cabytes.deref_mut()).await?;
        let ca = X509::from_pem(&cabytes)?;
        drop(cabytes);

        let (tx, rx) = channel(1);

        let _ = tokio::spawn(async move {
            handle_csrs(central, secret, key, ca, rx).await;
        });

        tx
    };

    run_server(addr, key_request_channel).await?;

    Ok(())
}

async fn sign_request<R: Rng + CryptoRng>(
    rng: &mut R,
    caddr: SocketAddr,
    secret: Secret,
    key: &PKey<Private>,
    ca: &X509,
    req: X509Req,
) -> anyhow::Result<X509> {
    // let's figure out the name and whether the user attached an email
    if req
        .subject_name()
        .entries_by_nid(Nid::PKCS9_EMAILADDRESS)
        .next()
        .is_none()
    {
        return Err(anyhow::Error::msg("No email addresses provided."));
    }
    let mut names = req.subject_name().entries_by_nid(Nid::COMMONNAME);
    let maybe_name = names.next();
    if names.next().is_some() {
        return Err(anyhow::Error::msg(
            "More than one Common Name (username) provided.",
        ));
    }
    let name = if let Some(name) = maybe_name {
        Username::from_str(&name.data().as_utf8()?)?
    } else {
        return Err(anyhow::Error::msg("Common Name (username) not specified."));
    };

    let mut builder = X509Builder::new()?;

    // x509v3
    builder.set_version(2)?;

    // set random serial
    let mut serial_bytes = Zeroizing::new([0u8; 20]);
    rng.fill_bytes(serial_bytes.deref_mut());
    let serial_bn = BigNum::from_slice(serial_bytes.deref())?;
    let serial = Asn1Integer::from_bn(&serial_bn)?;
    builder.set_serial_number(serial.as_ref())?;

    // set issuer
    builder.set_issuer_name(ca.issuer_name())?;

    // set validity to one week
    // TODO make a hard deadline
    let before = Asn1Time::from_unix(0)?;
    builder.set_not_before(before.as_ref())?;
    let after = Asn1Time::days_from_now(7)?;
    builder.set_not_after(after.as_ref())?;

    // force CN; everything else is up to them
    let mut name_builder = X509NameBuilder::new()?;
    for entry in req.subject_name().entries() {
        if entry.object().nid() != Nid::COMMONNAME
            && entry.object().nid() != Nid::PKCS9_EMAILADDRESS
            && !FIXED_NIDS.contains(&entry.object().nid())
        {
            name_builder
                .append_entry_by_nid(entry.object().nid(), entry.data().as_utf8()?.as_ref())?;
        }
    }
    name_builder.append_entry_by_nid(Nid::COMMONNAME, name.get_name())?;
    for nid in FIXED_NIDS {
        for entry in ca.subject_name().entries_by_nid(nid) {
            name_builder.append_entry_by_nid(nid, &entry.data().as_utf8()?)?;
        }
    }
    let x509name = name_builder.build();
    builder.set_subject_name(x509name.as_ref())?;

    // we must do SAN for webpki
    for email in req.subject_name().entries_by_nid(Nid::PKCS9_EMAILADDRESS) {
        let email = email.data().as_utf8()?.to_string();
        let parsed = addr::parse_email_address(&email)
            .map_err(|_| anyhow::Error::msg(format!("Invalid email address: {}", email)))?;
        let ext = X509Extension::new_nid(
            None,
            Some(&builder.x509v3_context(Some(ca.as_ref()), None)),
            Nid::SUBJECT_ALT_NAME,
            &format!("email:{}", parsed),
        )?;
        builder.append_extension(ext)?;
    }

    // public key
    builder.set_pubkey(req.public_key()?.as_ref())?;

    // sign that shit
    builder.sign(key, MessageDigest::sha256())?;

    // inform central: new user just dropped
    let mut conn = TcpStream::connect(caddr).await?;
    let mut user_secret = [0u8; 32];
    rng.fill_bytes(&mut user_secret);
    let _ = send_message(
        &mut conn,
        &Request::Registration(UserRegistration::new(name, user_secret), secret),
    )
    .await?;
    await_message::<_, RequestResult<()>>(&mut conn).await??;

    Ok(builder.build())
}

async fn handle_csrs(
    central: SocketAddr,
    secret: Secret,
    key: PKey<Private>,
    ca: X509,
    mut rx: Receiver<(X509Req, oneshot::Sender<anyhow::Result<X509>>)>,
) {
    let mut rng = ChaCha20Rng::from_entropy();
    while let Some((req, sender)) = rx.recv().await {
        let _ = sender.send(sign_request(&mut rng, central, secret, &key, &ca, req).await);
    }
}
